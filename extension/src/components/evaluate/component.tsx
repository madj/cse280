import React, { FunctionComponent } from "react";
import "./styles.scss";
import { browser, Tabs } from "webextension-polyfill-ts";
import { Chart } from "chart.js"
//const newWindowObject = window as any;
//let pushNotification = newWindowObject.Chart;

// // // //


/**
 * Executes a string of Javascript on the current tab
 * @param code The string of code to execute on the current tab
 */
async function executeScript(): Promise<void> {
    document.getElementById("spinner").innerHTML = "<div class='spinner-border text-warning'></div>"    //("spinnerButton").className = "spinner-border text-warning"
    document.getElementById("spinnerButton").style.display = "none";
    // Query for the active tab in the current window
    browser.tabs
        .query({ active: true, currentWindow: true })
        .then((tabs: Tabs.Tab[]) => {
            // Pulls current tab from browser.tabs.query response
            const currentTab: Tabs.Tab | undefined = tabs[0];

            // Short circuits function execution is current tab isn't found
            if (!currentTab) {
                return;
            }
           
            // Executes the script in the current tab
            browser.tabs
                .executeScript(currentTab.id, {
                    code: "var searchParam = window.location.href;"          //new URLSearchParams(window.location.href);"
                    + " var url = 'https://madjspam.live/api?url=' + searchParam;"
                    + " fetch(url)"
                    + ".then(data => {return data.json()})"   
                    + ".then(result => {console.log(result)})"
                })
                .then(() => {
                    document.getElementById('spinner').innerHTML = '';
                    document.getElementById('spinnerButton').style.display = 'block';
                    console.log("Done Scrolling");
                });
        });
        
}

async function sendRequest(){
    chrome.tabs.getSelected(null, async function(tab) {
        let spamNum = 0;
        let notSpamNum = 0;
        let i = 0;
        var tablink = tab.url;
        document.getElementById("spinner").innerHTML = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<div class='spinner-border text-warning'></div>"    //("spinnerButton").className = "spinner-border text-warning"
        document.getElementById("spinnerButton").style.display = "none";
        document.getElementById("minfo").style.display = "none";
        var url = 'https://madjspam.live/api?url=' + tablink;
        //alert(url)
        const response = await fetch(url)
        const json = await response.json()
       
        var info = json.information;
        info.forEach(countSpam);
        var data = [notSpamNum, spamNum];

        document.getElementById('spinner').innerHTML = '';


        chartGraph(data);

        document.getElementById('breakdown').style.alignContent = 'center';
        info.forEach(addSingle)
        document.getElementById("minfo").style.display = "block";
        function addSingle(arr){

           let name = arr.name;

            var arrSpamProb = arr.spamProbability;
            var arrNotSpam = 1 - arrSpamProb;

            let finData = [arrNotSpam, arrSpamProb];

            let node = document.createElement("canvas");
            node.setAttribute("id", "" + i);
            document.getElementById("breakdown").appendChild(node);

            var context = document.getElementById("" + i).getContext('2d');

            var singleChart = new Chart(context, {
                type: 'doughnut',
                data: {
                    datasets: [{
                        backgroundColor: ['green', 'red'],
                        data: finData
                    }],
                
                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: [
                        'Not Spam',
                        'Spam'
                    ]
                },
                options: {
                    rotation: 1 * Math.PI,
                    circumference: 1 * Math.PI,
                    title: {
                        display: true,
                        text: name,
                    },
                    layout:{
                        padding:{
                            left: 85
                        }
                    },
                    legend:{
                        display: false
                    }
                }
            });

            singleChart.canvas.parentNode.style.height = '30vh';
            singleChart.canvas.parentNode.style.width = '55vw';

            i += 1;
        }

        function countSpam(arr){
            if (arr.spamProbability < 0.5){
                notSpamNum += 1;
            }
            else{
                spamNum += 1;
            }
        }
    });
    
}



function chartGraph(data){
    var ctx = document.getElementById('myChart').getContext('2d');
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                backgroundColor: ['green', 'red'],
                data: data
            }],
        
            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Not Spam',
                'Spam'
            ]
        },
        options: {
            rotation: 1 * Math.PI,
            circumference: 1 * Math.PI,
            title:{
                display: true,
                fontColor: '#14668f',
                text: 'Aggregate'
            }
        }
    });
}

/*async function hideSpinner(){
    await sendRequest();
    document.getElementById('spinner').innerHTML = '';
    document.getElementById('spinnerButton').style.display = 'block';
}*/

function openTab(){
    var newURL = "http://madjspam.live:5000/";
    chrome.tabs.create({ url: newURL });
}


// // // //

export const Evaluate: FunctionComponent = () => {
    return (
        <div className="row">
            <div className="col-lg-12">
                <button
                    id = "spinnerButton"
                    className="btn btn-block btn-outline-warning"
                    onClick= {() => sendRequest()}
                    //onClick={(): void => executeScript(scrollToTopScript)}
                > 
                Evaluate
                </button>
                <button
                    id = "minfo"
                    className = "btn btn-block btn-outline-info"
                    onClick = {() => openTab()}
                >
                More Info    
                </button>
                <div id = "spinner">

                </div>
                <canvas id="myChart"></canvas>
                
                <div id = "breakdown">

                </div>
                
            </div>
        </div>
    );
};
