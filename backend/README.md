# MADJ Spam Detection Backend API

Public API: [https://madjspam.live/](https://madjspam.live/)

This uses [selenium](https://selenium-python.readthedocs.io/) with Python to scrape the yelp page with and [Flask](https://flask.palletsprojects.com/en/1.1.x/) as the backend API framework. We are also using a simple cache implemented using python dictionary and a cache naive cache file which works for our purposes.

I am also using Python3 to create a [virtual environment](https://docs.python.org/3/library/venv.html#venv-def) to create an isoalted environment to run the app on.

## Steps before running scraper:

- Create a virtualenv and run it. (This is slightly different for [Windows](https://programwithus.com/learn-to-code/Pip-and-virtualenv-on-Windows/) vs [Linux/Mac](https://www.pythonforbeginners.com/basics/how-to-use-python-virtualenv))
- In order for selenium to work, you need to download [`chromewebdriver`](https://sites.google.com/a/chromium.org/chromedriver/downloads) and place it into the `backend` directory containing the main python file. Choose the version that matches the web browser you have. Note: You can always opt to use a different browser like Firefox. Just make sure to change the code accordingly to reflect that, namely this part:
```python
CHROMEDRIVER_PATH = './chromedriver.exe'
```

- Run `pip install -r requirements.txt` from the inside the directory containing `requirements.txt` file while the virtual environment is running to install all the dependencies.

## Running the Server

Locally, while inside the `backend` directory and while your virtual environment is running, run 
```bash
python3 server.py
```
Make sure all the requirements are installed and you have the right version of chromedriver before you run this python file.

## Pushing the code updates to Cloud Server

After you are done making changes/updates to `server.py` and feel like it's time to push those changes to the cloud web server, follow these steps:

- Update this change onto `madj_production.py` since this is the actual python file that is used in the web server
- SSH into the Cloud Server and cd in the `madj` directory then update the changes you made to the `madj.py` file in there. (**Note:** Do not change the name of this file!)
- This above step can be accomplished quickly and easily using [FileZilla](https://filezilla-project.org/) (Replace the `madj.py` with `madj_production.py` then rename this new file to `madj.py` 😁)
- You can quickly test this new change by doing this:
```bash
# starting at root directory
source madj/madj/bin/activate #start the virtual environment
cd madj 
python3 madj.py #running the python file
```
- Go to [http://madjspam.live:5000/](http://madjspam.live:5000/) to test this
- Once you are happy, stop the server and `deactivate` the virtual environment
- To push to full production run:
```bash
sudo systemctl restart nginx #restart nginx service
sudo systemctl restart madj.service #restart our main web server service
```
- Now you can go to [https://madjspam.live/](https://madjspam.live/), the main domain, and see the changes reflected there too!

## Helpful Tips
This section is highly detailed in the *Project Summary* document. Contact the course admin to get access to it.
Some helpful links we used:
- [How To Serve Flask Applications with uWSGI and Nginx on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uswgi-and-nginx-on-ubuntu-18-04)
- [Setting up a Digital Ocean server for Selenium, Chrome, and Python](http://jonathansoma.com/lede/algorithms-2017/servers/setting-up/)
