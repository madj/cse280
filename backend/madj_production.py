from flask import Flask, request, jsonify
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from pyvirtualdisplay import Display
import logging
import pickle
import json

# init flask app
app = Flask(__name__)

# start the virtual display for selenium
display = Display(visible=0, size=(800, 600))
display.start()

# options to pass to selenium driver instance
options = webdriver.ChromeOptions()
options.add_argument('--no-sandbox')
options.headless = True

# Load model from pickled file
pickled_file = open('model.pickle', 'rb')
model = pickle.load(pickled_file)
pickled_file.close()

# cache file
cache = {}

with open('cache', 'r') as cache_file:
    cache = json.loads(cache_file.read())


def predictor(pr, nr):
    # function to do the prediction
    return model.predict_proba([[pr, nr]])


# crawling each reviewer to get their metadata
def scrape_reviewer(reviewers_list, driver, biz_url):
    info = []
    all_users = []
    for reviewer_href in reviewers_list:
        meta_data = {}
        curr_key = 'user:'+reviewer_href+'business:' + biz_url
        if curr_key in cache:
            entry = cache[curr_key]
            meta_data = {
                'name': entry['name'],
                'notSpamProbability': entry['notSpamProbability'],
                'spamProbability': entry['spamProbability']
            }
        else:
            driver.get(reviewer_href)
            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.ID, 'super-container')))
            username = driver.find_element_by_css_selector(
                '.user-profile_info.arrange_unit > h1')
            username = username.text
            total_reviews = driver.find_element_by_css_selector(
                'li.review-count > strong')
            total_reviews = int(total_reviews.text)
            all_reviews = driver.find_elements_by_class_name('histogram_count')
            pos_reviews = 0
            neg_reviews = 0

            for idx in range(len(all_reviews)):
                curr_score = int(all_reviews[idx].text)
                if idx == 3 or idx == 4:
                    neg_reviews += curr_score
                else:
                    pos_reviews += curr_score

            pos_ratio = pos_reviews / total_reviews
            neg_ratio = neg_reviews / total_reviews

            [[not_spam_prob, spam_prob]] = predictor(pos_ratio, neg_ratio)

            print(username)
            if (pos_reviews + neg_reviews) <= 3:
                meta_data = {
                    'name': username,
                    'notSpamProbability': 0.10,
                    'spamProbability': 0.90
                }
            else:
                meta_data = {
                    'name': username,
                    'notSpamProbability': not_spam_prob,
                    'spamProbability': spam_prob
                }
            cache[curr_key] = meta_data
        info.append(meta_data)

    for elem in info:
        all_users.append(elem['name'])

    return {
        'users': all_users,
        'information': info
    }

# run selenium with chrome browser for every network request that is made


def run_selenium(url):
    driver = webdriver.Chrome(options=options)
    driver.get(url)
    WebDriverWait(driver, 10).until(EC.presence_of_element_located(
        (By.CSS_SELECTOR, 'li > div > div > div > div > div > div > div > div > span > a')))
    reviewers = driver.find_elements_by_css_selector(
        'li > div > div > div > div > div > div > div > div > span > a')
    reviewers_links = list(
        map(lambda element: element.get_attribute('href'), reviewers))
    user_metadata = {'err': 'Error while crawling each reviewer'}
    try:
        user_metadata = scrape_reviewer(reviewers_links, driver, url)
    except:
        logging.exception('Error occured while crawling each reviwer link!')
    finally:
        with open('cache', 'w') as cache_file:
            json.dump(cache, cache_file)
        driver.quit()
        return user_metadata


@app.route('/api', methods=['GET'])
def analyzer():
    # get url from request
    url = request.args.get('url')
    # check if the url is valid
    if url is None or 'https://www.yelp.com/biz' not in str(url):
        return 'Invalid url recieved!', 400
    # run selenium with the url just passed in
    user_data = run_selenium(url)
    user_data = jsonify(user_data)
    user_data.headers.add('Access-Control-Allow-Origin', '*')
    # return the json of the response that we got
    return user_data, 200


@app.route("/")
def hello():
    return "<html><h1 style='color:blue;text-align:center;'>Welcome to MADJ spam API!</h1><body style='color:black;text-align:center;'>Make a request to the API in the url https://madjspam.live/api?url=[yelp_url_of_business]. For instance, send a API request to https://madjspam.live/api?url=https://www.yelp.com/biz/the-goose-bethlehem where the url is the yelp business page for \"The Goose\" in Bethlehem, PA.</body></html>"


if __name__ == "__main__":
    try:
        app.run(host='0.0.0.0')
    finally:
        display.stop()
